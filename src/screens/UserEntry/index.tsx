/*
    Displays user entry form
*/

import React, {Component} from 'react';
import {ActivityIndicator, Button, Text, TextInput, View} from 'react-native';
import {userDataKeys} from '../../../keys';
import styles from './styles';
import UserStore from '../../core/store/user-store';
import {stateType} from '../../core/types/user-redux-types';

const store = UserStore.getInstance().userStore;

class UserEntry extends Component<any, stateType> {
  private unsubscribe: any = null;

  constructor(props: any) {
    super(props);
    this.state = store.getState();
  }

  componentDidMount() {
    this.unsubscribe = store.subscribe(() => {
      this.setState(store.getState());

      this.checkSubmitComplete();
    });
  }

  checkSubmitComplete = () => {
    if (
      this.state.error === null &&
      !this.state.loading &&
      !this.state.initiated
    ) {
      this.props.navigation.navigate('Home');
    }
  };

  render() {
    const {onUserInfoUpdate} = this.props;
    return (
      <View style={styles.parent}>
        <Text style={styles.inputLabel}>Name:</Text>
        <TextInput
          style={styles.inputField}
          value={this.state.user.name}
          onChangeText={v => onUserInfoUpdate(userDataKeys.name, v)}
          placeholder="Enter your name"
          placeholderTextColor="grey"
        />
        <Text style={styles.inputLabel}>Age:</Text>
        <TextInput
          style={styles.inputField}
          value={this.state.user.age}
          onChangeText={v => onUserInfoUpdate(userDataKeys.age, v)}
          keyboardType="numeric"
          placeholder="Enter your age"
          placeholderTextColor="grey"
        />
        {this.state.error ? (
          <Text style={styles.errorText}>{this.state.error}</Text>
        ) : null}
        {this.state.loading ? (
          <ActivityIndicator />
        ) : (
          <View style={{marginTop: 16}}>
            <Button onPress={this.props.onClickSubmit} title="Submit" />
          </View>
        )}
      </View>
    );
  }

  componentWillUnmount() {
    if (this.unsubscribe) {
      this.unsubscribe();
    }
  }
}

export default UserEntry;
