import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  parent: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputField: {
    width: '90%',
    margin: 8,
    color: 'black',
    backgroundColor: '#d6d6d6',
    borderRadius: 4,
  },
  inputLabel: {
    width: '90%',
    marginHorizontal: 8,
    color: 'grey',
    textAlign: 'left',
  },
  errorText: {
    margin: 8,
    color: 'red',
  },
});

export default styles;
