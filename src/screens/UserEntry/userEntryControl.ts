/*
    Controls user data entry ui
*/
import {userEntryControllerReturn} from '../../core/types/controlle-type';
import {updateUserAtStore, writeUserToDb} from '../../core/actions/user-action';
import UserStore from '../../core/store/user-store';

const store = UserStore.getInstance().userStore;

export default function UserEntryController(): userEntryControllerReturn {
  const onClickSubmit = async () => {
    await writeUserToDb();
  };

  const onUserInfoUpdate = (key: string, value: string) => {
    const tempUser = store.getState().user;
    tempUser[key] = value;
    updateUserAtStore({...tempUser});
  };

  return {
    onClickSubmit,
    onUserInfoUpdate,
  };
}
