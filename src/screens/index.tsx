import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import UserEntry from './UserEntry';
import UserEntryController from './UserEntry/userEntryControl';
import Home from './Home';
import HomeController from './Home/homeControl';
import LoginController from './Login/loginControl';
import Login from './Login';

const RootStack = createStackNavigator();

type screenNavigationProp = {initialRouteName: string};

export default function ScreenNavigation(navProps: screenNavigationProp) {
  return (
    <NavigationContainer>
      <RootStack.Navigator initialRouteName={navProps.initialRouteName}>
        <RootStack.Screen
          name="UserEntry"
          children={props => (
            <UserEntry {...props} {...UserEntryController()} />
          )}
          options={{
            headerTitle: 'Welcome Page',
            headerLeft: () => <></>,
            gestureEnabled: false,
          }}
        />
        <RootStack.Screen
          name="Home"
          children={props => <Home {...props} {...HomeController()} />}
          options={{
            headerTitle: 'Welcome Page',
            headerLeft: () => <></>,
            gestureEnabled: false,
          }}
        />
        <RootStack.Screen
          name="Login"
          children={props => <Login {...props} {...LoginController()} />}
          options={{headerShown: false}}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  );
}
