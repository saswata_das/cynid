import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  parent: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  userSignStatusCheck: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 24,
  },
  googleSignFail: {
    backgroundColor: 'red',
    color: 'white',
    textAlign: 'center',
    textAlignVertical: 'center',
    padding: 8,
    margin: 8,
  },
  logo: {
    height: 48,
    width: 48,
    borderRadius: 48,
    margin: 16,
  },
});

export default styles;
