/*
    Controls login screen ui
*/

import {GoogleSignin} from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';
import {loginControllerReturn} from '../../core/types/controlle-type';

export default function LoginController(): loginControllerReturn {
  const onClickGoogleSignIn = async (): Promise<boolean> => {
    try {
      GoogleSignin.configure({
        webClientId:
          '302617783837-a1uiip5l7igogf9tcq3us062k1k1qso6.apps.googleusercontent.com',
      });
      const {idToken} = await GoogleSignin.signIn();
      const googleCredential = auth.GoogleAuthProvider.credential(idToken);
      await auth().signInWithCredential(googleCredential);
      return true;
    } catch (e) {
      console.log(e);
      return false;
    }
  };

  const isUserSigned = async (): Promise<boolean> => {
    return await GoogleSignin.isSignedIn();
  };

  return {
    isUserSigned,
    onClickGoogleSignIn,
  };
}
