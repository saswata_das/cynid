/*
    Displays login option
*/

import React, {useEffect, useState} from 'react';
import {ActivityIndicator, Button, Image, Text, View} from 'react-native';
import styles from './styles';

export default function Login(props: any) {
  const {isUserSigned, onClickGoogleSignIn} = props;
  const [googleSignFail, setGoogleSignFail] = useState(false);
  const [loginStatus, setLoginStatus] = useState(true);

  useEffect(() => {
    init();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const init = async () => {
    if (await isUserSigned()) {
      props.navigation.navigate('Home');
    }
    setLoginStatus(false);
  };

  const googleSignHandler = async () => {
    setLoginStatus(true);
    setGoogleSignFail(false);
    if (await onClickGoogleSignIn()) {
      props.navigation.navigate('UserEntry');
    } else {
      setGoogleSignFail(true);
    }
    setLoginStatus(false);
  };

  return (
    <View style={styles.parent}>
      <Image
        style={styles.logo}
        source={{
          uri: 'http://lofrev.net/wp-content/photos/2017/03/user_blue_logo.png',
        }}
      />
      {loginStatus ? (
        <View style={styles.userSignStatusCheck}>
          <ActivityIndicator />
          <Text style={{color: 'grey'}}>Checking if user logged in</Text>
        </View>
      ) : (
        <Button onPress={googleSignHandler} title="Signin with Google" />
      )}
      {googleSignFail ? (
        <Text style={styles.googleSignFail}>Google login failed</Text>
      ) : null}
    </View>
  );
}
