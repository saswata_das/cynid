import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  parent: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  userInfo: {
    margin: 8,
    color: 'grey',
  },
  errorText: {
    margin: 8,
    color: 'red',
  },
});

export default styles;
