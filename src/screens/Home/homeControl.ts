/*
    Controls user data display ui
*/

import {readUserFromDb} from '../../core/actions/user-action';
import {homeControllerReturn} from '../../core/types/controlle-type';

export default function HomeController(): homeControllerReturn {
  const getUserData = async () => {
    await readUserFromDb();
  };
  return {
    getUserData,
  };
}
