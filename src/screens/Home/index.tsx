/*
    Displays user data
*/
import React, {Component} from 'react';
import {ActivityIndicator, Text, View} from 'react-native';
import UserStore from '../../core/store/user-store';
import {homeControllerReturn} from '../../core/types/controlle-type';
import {stateType} from '../../core/types/user-redux-types';
import styles from './styles';

const store = UserStore.getInstance().userStore;

class Home extends Component<homeControllerReturn, stateType> {
  private unsubscribe: any = null;

  constructor(props: homeControllerReturn) {
    super(props);
    this.state = store.getState();
  }

  displayUserData = () => {
    this.setState({...this.state, loading: true});
    this.props.getUserData();
    this.setState({...this.state, loading: false});
  };

  componentDidMount() {
    this.unsubscribe = store.subscribe(() => {
      this.setState(store.getState());
    });
    this.displayUserData();
  }

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.parent}>
          <ActivityIndicator />
        </View>
      );
    } else if (this.state.error) {
      return (
        <View style={styles.parent}>
          <Text style={styles.errorText}>{this.state.error}</Text>
        </View>
      );
    } else {
      return (
        <View style={styles.parent}>
          <Text style={styles.userInfo}>Name: {this.state.user.name}</Text>
          <Text style={styles.userInfo}>Age: {this.state.user.age}</Text>
        </View>
      );
    }
  }

  componentWillUnmount() {
    if (this.unsubscribe) {
      this.unsubscribe();
    }
  }
}

export default Home;
