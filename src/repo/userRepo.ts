/*
    Reads and writes user data
*/

import {User} from '../core/types/user-type';
import firestore from '@react-native-firebase/firestore';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  convertUserFireToStore,
  convertUserStoreToFire,
} from '../core/dto/user-dto';
import {localStoreKeys} from '../../keys';

type createUserReturn = Promise<{
  user: User;
  success: boolean;
  error: String | null;
}>;
type fetchUserReturn = Promise<{
  user: User;
  success: boolean;
  error: String | null;
}>;

export async function createUser(user: User): createUserReturn {
  try {
    const userDoc: any = await firestore()
      .collection('Users')
      .add(convertUserStoreToFire(user));
    if (await writeUserIdToLocalStore(userDoc.id)) {
      return {
        user: convertUserFireToStore(userDoc.id, user),
        success: true,
        error: null,
      };
    } else {
      return {user, success: false, error: 'Registration unsussesful'};
    }
  } catch (e) {
    return {user, success: false, error: 'Registration unsussesful'};
  }
}

async function writeUserIdToLocalStore(id: string): Promise<boolean> {
  try {
    await AsyncStorage.setItem(localStoreKeys.id, id);
    return true;
  } catch (e) {
    console.log(e);
    return false;
  }
}

async function readUserIdFromLocalStore(): Promise<string | false> {
  try {
    const value = await AsyncStorage.getItem(localStoreKeys.id);
    if (value !== null) {
      return value;
    } else {
      return false;
    }
  } catch (e) {
    console.log(e);
    return false;
  }
}

export async function fetchUser(): fetchUserReturn {
  try {
    const id = await readUserIdFromLocalStore();
    if (id) {
      const userDoc: any = await firestore().collection('Users').doc(id).get();
      return {
        user: {id: id, name: userDoc._data.name, age: userDoc._data.age},
        success: true,
        error: null,
      };
    } else {
      return {
        user: {id: null, name: '', age: ''},
        success: false,
        error: 'User id not found',
      };
    }
  } catch (e) {
    console.log(e);
    return {
      user: {id: null, name: '', age: ''},
      success: false,
      error: 'User not found',
    };
  }
}
