/*
  Reducers for user actions
*/

import {actionTypes, stateType} from '../types/user-redux-types';

const intitalState: stateType = {
  initiated: true,
  loading: false,
  user: {
    id: null,
    name: '',
    age: '',
  },
  error: null,
};

export default function userReducer(
  state: stateType = intitalState,
  action: actionTypes,
): stateType {
  switch (action.type) {
    case 'read_db':
      return {
        ...state,
        loading: false,
        user: action.payload,
      };
    case 'write_db':
      return {
        ...state,
        user: action.payload,
        loading: false,
        error: null,
        initiated: false,
      };
    case 'write_store':
      return {
        ...state,
        user: action.payload,
      };
    case 'fix':
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case 'loading':
      return {
        ...state,
        loading: true,
        error: null,
      };
    case 'error':
    default:
      return state;
  }
}
