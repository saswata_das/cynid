import {User} from '../types/user-type';

export function convertUserFireToStore(documentId: string, user: User): User {
  user.id = documentId;
  return user;
}

export function convertUserStoreToFire(user: User) {
  return {
    name: user.name,
    age: user.age,
  };
}
