/*
    Stores app user data
*/

import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import userReducer from '../reducers/user-reducer';

export default class UserStore {
  private static instance: UserStore;
  public userStore;

  private constructor() {
    this.userStore = createStore(userReducer, applyMiddleware(thunk));
  }

  public static getInstance(): UserStore {
    if (!UserStore.instance) {
      UserStore.instance = new UserStore();
    }

    return UserStore.instance;
  }
}
