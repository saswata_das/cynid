export function isEmptyOrNull(value: any) {
  return value === undefined || value === null || value === '';
}
export function isZeroOrNull(value: any) {
  return value === undefined || value === null || value === 0;
}
