/*
    Manages business logic and side effects
*/

import {createUser, fetchUser} from '../../repo/userRepo';
import UserStore from '../store/user-store';
import {User} from '../types/user-type';
import {isEmptyOrNull, isZeroOrNull} from '../utils/user-utils';

type dispatchParam = (props: {type: string; payload: any}) => void;

const store = UserStore.getInstance().userStore;

export async function writeUserToDb() {
  store.dispatch<any>(async (dispatch: dispatchParam, getState: () => any) => {
    const user = getState().user;
    if (!isEmptyOrNull(user.name) && !isZeroOrNull(user.age)) {
      dispatch({
        type: 'loading',
        payload: null,
      });
      const res = await createUser(user);
      if (res.success) {
        dispatch({
          type: 'write_db',
          payload: res.user,
        });
      } else {
        dispatch({
          type: 'error',
          payload: res.error,
        });
      }
    } else {
      dispatch({
        type: 'fix',
        payload: 'Please fill all fields',
      });
    }
  });
}

export async function readUserFromDb() {
  store.dispatch<any>(async (dispatch: dispatchParam) => {
    const res = await fetchUser();
    if (
      res.success &&
      !isEmptyOrNull(res.user.name) &&
      !isZeroOrNull(res.user.age) &&
      !isEmptyOrNull(res.user.id)
    ) {
      dispatch({
        type: 'read_db',
        payload: res.user,
      });
    } else {
      dispatch({
        type: 'fix',
        payload: res.error,
      });
    }
  });
}

export function updateUserAtStore(user: User) {
  store.dispatch<any>(async (dispatch: dispatchParam) => {
    dispatch({
      type: 'write_store',
      payload: user,
    });
  });
}
