/*
    Maintains user type definations
*/

export type User = {
  [key: string]: any;
  id: string | null;
  name: string;
  age: string;
};
