/*
    Maintains action type definations
*/

import {User} from './user-type';

export type actionTypes = {
  type: 'read_db' | 'write_db' | 'write_store' | 'error' | 'fix' | 'loading';
  payload: any;
};

export type stateType = {
  initiated: boolean;
  loading: boolean;
  user: User;
  error: String | null;
};
