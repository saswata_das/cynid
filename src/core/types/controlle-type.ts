export type homeControllerReturn = {
  getUserData: () => Promise<void>;
};

export type loginControllerReturn = {
  isUserSigned: () => Promise<boolean>;
  onClickGoogleSignIn: () => Promise<boolean>;
};

export type userEntryControllerReturn = {
  onUserInfoUpdate: (key: string, value: any) => void;
  onClickSubmit: () => Promise<void>;
};
