import {StyleSheet} from 'react-native';

const AppStyles = StyleSheet.create({
  appParent: {
    width: '100%',
    height: '100%',
  },
});

export {AppStyles};
