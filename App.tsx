import React, {useEffect, useState} from 'react';
import {Alert, StatusBar} from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {AppStyles} from './app-styles';
import messaging from '@react-native-firebase/messaging';
import ScreenNavigation from './src/screens/index';

const App = () => {
  const [inititalScreen, setInitialScreen] = useState('Login');

  const onMessageReceived = async (message: any) => {
    if (message) {
      if (message.data.screen) {
        setInitialScreen(message.data.screen);
      }
      Alert.alert(message.notification.title, message.notification.body);
    }
  };

  const initFirebase = async () => {
    requestUserPermission();

    // messaging()
    //   .getIsHeadless()
    //   // eslint-disable-next-line @typescript-eslint/no-unused-vars
    //   .then(isHeadless => {
    //     // do sth with isHeadless
    //   });

    messaging().onMessage(onMessageReceived);
    messaging().setBackgroundMessageHandler(onMessageReceived);
    messaging().onNotificationOpenedApp(onMessageReceived);
    messaging().getInitialNotification().then(onMessageReceived);
  };

  async function requestUserPermission() {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      console.log('Authorization status:', authStatus);
    }
  }

  useEffect(() => {
    initFirebase();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <SafeAreaProvider>
      <SafeAreaView style={AppStyles.appParent}>
        <StatusBar
          animated={true}
          barStyle="default"
          showHideTransition="slide"
          hidden={false}
        />
        <ScreenNavigation initialRouteName={inititalScreen} />
      </SafeAreaView>
    </SafeAreaProvider>
  );
};

export default App;
